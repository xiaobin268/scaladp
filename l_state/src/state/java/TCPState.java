package state.java;

public enum TCPState {
    Listening, Established, Closed;

    public void open(Server svr){
        System.out.println("opening");
        switch(this){
            case Listening:
                // do open
                break;
            default:
        }
        svr.setState(Established);
        System.out.println("closed");
    }
    public void close(Server svr){
        System.out.println("closing");
        switch (this){
            case Established:
                // do close
                break;
            default:
        }
        svr.setState(Closed);
        System.out.println("closed");
    }
}

class Server {
    private TCPState state = TCPState.Listening;

    public Server open(){
        state.open(this);
        return this;
    }
    public Server close(){
        state.close(this);
        return this;
    }

    void setState(TCPState state) {
        this.state = state;
    }
}

class Client {
    public static void main(String[] args) {
        Server svr = new Server();
        svr.open().close();
        svr.close().open();
    }
}
