package factory_method.java;

public class Orange implements IFruit {

	private boolean mature; 
	
	@Override
	public String getTaste() {
		if(mature) 
		return "bad";
		else return "good";
	}
	
	public Orange(boolean mature){
		this.mature = mature;
	}

}
