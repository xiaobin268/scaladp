package adapter.java;

import java.util.ArrayList;
import java.util.List;

public class Client {
	public static void main(String[] args) {
		List<IShape> shapes = new ArrayList<>();
		shapes.add(new Rectangle());
		shapes.add(new TextShape("abc"));
		for(IShape shape: shapes)
			shape.draw();
	}
}
