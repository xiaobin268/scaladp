package adapter.java.partial;

public interface IMouseListener {
    public void onLButtonMove();
    public void onLButtonDown();
    public void onLButtonUp();
}

class Client implements IMouseListener {

    @Override
    public void onLButtonMove() {
    }

    @Override
    public void onLButtonDown() {
    }

    @Override
    public void onLButtonUp() {
    }
}

abstract class MouseLisenerAdapter implements IMouseListener{
    @Override
    public void onLButtonMove() {
    }

    @Override
    public void onLButtonDown() {
    }

    @Override
    public void onLButtonUp() {
    }
}
class Client2 extends MouseLisenerAdapter {
    @Override
    public void onLButtonDown() {
        super.onLButtonDown();
    }
}

