package typeclass.scala

trait Reversible[T] {
  def rev(v: T):T
}

object Reversibles {
  implicit object StringIsReversable extends Reversible[String]{
    def rev(v: String): String = v.reverse
  }

  implicit object IntIsReversible extends Reversible[Int]{
    def rev(v: Int): Int = ~v
  }
}
